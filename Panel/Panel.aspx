﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Panel.aspx.cs" Inherits="Panel.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            height: 23px;
        }
        .auto-style2 {
            width: 100px;
            height: 26px;
        }
        .auto-style3 {
            height: 26px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <table style="width: 100%;">
            <tr>
                <td style="text-align:center" class="auto-style1">
                    <strong><span style="font-size: 18pt; color: #000000">Controle Panel</span></strong>
                </td>
            </tr>
            <tr>
                <td style="width:100px">
                    <asp:Panel ID="Panel1" runat="server">
                        <asp:Panel ID="Panel2" runat="server">
                            <table style="border-right: maroon thin solid; border-top: maroon thin solid; border-left: maroon thin solid; border-bottom: maroon thin solid">
                                <tr>
                                    <td colspan="2"; style="text-align:center; border-bottom: maroon thin solid" >
                                        <strong><span style="font-size: 16px;">Informações Pessoais:</span></strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px; text-align:right;">Nome:</td>
                                    <td>
                                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:100px; text-align:right;">Sobrenome:</td>
                                    <td>
                                        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:100px; text-align:right;">Gênero:</td>
                                    <td>
                                        <asp:DropDownList ID="DropDownList1" runat="server">
                                            <asp:ListItem Value="cis" Text="Cisgênero"></asp:ListItem>
                                            <asp:ListItem Value="Trans" Text="Transgênero"></asp:ListItem>
                                            <asp:ListItem Value="bin" Text="Não Binário"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:100px; text-align:right;">Celular:</td>
                                    <td>
                                        <asp:TextBox ID="TextBox3" TextMode="Phone" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:100px; height: 26px"></td>
                                    <td>
                                        <asp:Button ID="Button1" runat="server" Text="Próximo" OnClick="Button1_Click" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="Panel3" Visible="false" runat="server">
                            <table style="border: thin solid #FFFF00">
                                <tr>
                                    <td colspan="2"; style="text-align:center; border-bottom:thin solid #FFFF00" >
                                        <strong><span style="font-size: 16px;">Detalhes do Endereço:</span></strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:100px; text-align:right;">Endereço:</td>
                                    <td>
                                        <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:100px; text-align:right;">Cidade:</td>
                                    <td>
                                        <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:100px; text-align:right;">Cep:</td>
                                    <td>
                                        <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:100px; height: 26px"></td>
                                    <td>
                                        <asp:Button ID="Button2" runat="server" Text="Volta" Width="65px" OnClick="Button2_Click"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:100px; height: 26px"></td>
                                    <td>
                                        <asp:Button ID="Button3" runat="server" Text="Próximo" OnClick="Button3_Click" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="Panel4" Visible="false" runat="server">
                            <table style="border: thin solid #00FFFF">
                                <tr>
                                    <td colspan="2"; style="text-align:center; border-bottom:thin solid #00FFFF" >
                                        <strong><span style="font-size: 16px;">Área de Login:</span></strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:100px; text-align:right;">Usuário:</td>
                                    <td>
                                        <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:100px; text-align:right;">Senha:</td>
                                    <td>
                                        <asp:TextBox ID="TextBox8" TextMode="Password" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style2"></td>
                                    <td class="auto-style3">
                                        <asp:Button ID="Button4" runat="server" Text="Volta" Width="65px" OnClick="Button4_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:100px; height: 26px"></td>
                                    <td>
                                        <asp:Button ID="Button5" runat="server" Text="Enviar" Width="65px" OnClick="Button5_Click"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:100px">
                                        <asp:Label ID="envio" Visible="false" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
